package com.ushakov.tm.model;

import com.ushakov.tm.api.model.IWBS;
import com.ushakov.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractOwnerBusinessEntity implements IWBS {


    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ID: " + this.getId() + "\nNAME: " + name + "\nDESCRIPTION: " + description
                + "\nSTATUS: " + status.getDisplayName() + "\nSTART DATE: " + dateStart
                + "\nCREATED: " + created;
    }

}
