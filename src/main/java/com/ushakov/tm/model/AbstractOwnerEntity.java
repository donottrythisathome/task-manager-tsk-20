package com.ushakov.tm.model;

import com.ushakov.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractOwnerEntity extends AbstractEntity{

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
