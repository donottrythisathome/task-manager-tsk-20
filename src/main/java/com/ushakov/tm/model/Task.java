package com.ushakov.tm.model;

import com.ushakov.tm.api.model.IWBS;
import com.ushakov.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractOwnerBusinessEntity implements IWBS {

    public String projectId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "ID: " + this.getId() + "\nNAME: " + name + "\nDESCRIPTION: " + description
                + "\nSTATUS: " + status.getDisplayName() + "\nPROJECT ID:" + projectId
                + "\nSTART DATE: " + dateStart + "\nCREATED: " + created;
    }

}
