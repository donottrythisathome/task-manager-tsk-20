package com.ushakov.tm.bootstrap;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.api.service.*;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.command.auth.*;
import com.ushakov.tm.command.project.*;
import com.ushakov.tm.command.system.*;
import com.ushakov.tm.command.task.*;
import com.ushakov.tm.command.user.*;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.system.UnknownCommandException;
import com.ushakov.tm.repository.CommandRepository;
import com.ushakov.tm.repository.ProjectRepository;
import com.ushakov.tm.repository.TaskRepository;
import com.ushakov.tm.repository.UserRepository;
import com.ushakov.tm.service.*;
import com.ushakov.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByNameCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectDeleteByIdCommand());
        registry(new ProjectFindByIdCommand());
        registry(new ProjectFindByIndexCommand());
        registry(new ProjectFindByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindByProjectIdCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByNameCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskFindByIdCommand());
        registry(new TaskFindByIndexCommand());
        registry(new TaskFindByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserRegistrationCommand());
        registry(new UserFindByLoginCommand());
        registry(new UserFindByIdCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserShowListCommand());
        registry(new UserSetPasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());

        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    private void initUsers() {
        userService.add("user", "user", "try@mail.ru");
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        loggerService.debug("EXAMPLE");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[SUCCESS]\n");
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]\n");
            }
        }
    }

}
