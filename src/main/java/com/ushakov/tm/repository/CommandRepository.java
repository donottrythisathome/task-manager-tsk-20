package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(final AbstractCommand command) {
        list.add(command);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        for (final AbstractCommand command : list) {
            if (arg.equals(command.arg())) return command;
        }
        return null;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        for (final AbstractCommand command : list) {
            if (name.equals(command.name())) return command;
        }
        return null;
    }

    @Override
    public List<String> getCommandNames() {
        final List<String> names = new ArrayList<>();
        for (final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return null;
    }

}
