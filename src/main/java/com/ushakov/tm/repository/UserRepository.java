package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByEmail(final String email) {
        for (final User user : list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        remove(user);
        return user;
    }

}
