package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractOwnerBusinessRepository<Project> implements IProjectRepository {
}
