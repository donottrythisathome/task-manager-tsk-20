package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        final List<E> result = findAll(userId);
        this.list.removeAll(result);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()))
                entities.add(entity);
        }
        return entities;
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        final List<E> entities = findAll(userId);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findOneById(final String userId, final String id) {
        for (final E entity : list) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final E entity = findOneById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        remove(entity);
        return entity;
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        try {
            final E entity = list.get(index);
            if (userId.equals(entity.getUserId())) return entity;
            return null;
        } catch (Exception e) {
            throw new ObjectNotFoundException();
        }
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        remove(entity);
        return entity;
    }

}
