package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractOwnerBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> projectTaskList = new ArrayList<>();
        for (final Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) projectTaskList.add(task);
        }
        return projectTaskList;
    }

}
