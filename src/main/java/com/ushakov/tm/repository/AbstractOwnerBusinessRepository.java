package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerBusinessRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;

public abstract class AbstractOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity>
        extends AbstractOwnerRepository<E>
        implements IOwnerBusinessRepository<E> {

    @Override
    public E findOneByName(final String userId, final String name) {
        for (final E entity : list) {
            if (!userId.equals(entity.getUserId())) continue;
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final E entity = findOneByName(userId, name);
        if (entity == null) throw new ObjectNotFoundException();
        remove(entity);
        return entity;
    }

}
