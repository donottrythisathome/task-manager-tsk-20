package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String value) {
        super("Error! Unknown command: " + value + "!");
    }

}
