package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear task list.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear(userId);
    }

    @Override
    public String name() {
        return "task-clear";
    }

}
