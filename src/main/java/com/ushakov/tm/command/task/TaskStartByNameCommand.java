package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startTaskByName(userId, taskName);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "start-task-by-name";
    }

}
