package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String taskDescription = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().add(userId, taskName, taskDescription);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "task-create";
    }

}
