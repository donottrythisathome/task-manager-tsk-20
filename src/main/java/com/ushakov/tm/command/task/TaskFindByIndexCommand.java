package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskFindByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
        System.out.println(task);
    }

    @Override
    public String name() {
        return "find-task-by-index";
    }

}
