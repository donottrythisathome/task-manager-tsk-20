package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        final int taskIndex = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().updateTaskByIndex(userId, taskIndex - 1, taskName, taskDescription) == null) {
            throw new TaskNotFoundException();
        }
    }

    @Override
    public String name() {
        return "update-task-by-index";
    }

}
