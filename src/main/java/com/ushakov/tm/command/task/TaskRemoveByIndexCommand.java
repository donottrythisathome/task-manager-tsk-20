package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "remove-task-by-index";
    }

}
