package com.ushakov.tm.command;

import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
        System.out.println("Login: " + user.getLogin());
    }

}
