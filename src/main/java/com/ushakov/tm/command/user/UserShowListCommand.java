package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.model.User;

import java.util.List;

public class UserShowListCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Override
    public String name() {
        return "user-list";
    }

}
