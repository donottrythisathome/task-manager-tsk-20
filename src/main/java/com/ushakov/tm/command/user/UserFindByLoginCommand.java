package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;

public class UserFindByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        System.out.println(user);
    }

    @Override
    public String name() {
        return "user-find-by-login";
    }

}
