package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT INDEX");
        final int projectIndex = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().updateProjectByIndex(userId, projectIndex - 1, projectName, projectDescription) == null) {
            throw new ProjectNotFoundException();
        }
    }

    @Override
    public String name() {
        return "update-project-by-index";
    }

}
