package com.ushakov.tm.api.service;

import com.ushakov.tm.model.AbstractOwnerBusinessEntity;

public interface IOwnerBusinessService<E extends AbstractOwnerBusinessEntity> extends IOwnerService<E> {

    E findOneByName(final String userId, final String name);

    E removeOneByName(final String userId, final String name);

}
