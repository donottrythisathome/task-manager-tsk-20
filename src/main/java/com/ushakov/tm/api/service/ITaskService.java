package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerBusinessService<Task> {

    Task add(String userId, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

    Task completeTaskById(String userId, String id);

    Task completeTaskByIndex(String userId, Integer index);

    Task completeTaskByName(String userId, String name);

    Task startTaskById(String userId, String id);

    Task startTaskByIndex(String userId, Integer index);

    Task startTaskByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

}
