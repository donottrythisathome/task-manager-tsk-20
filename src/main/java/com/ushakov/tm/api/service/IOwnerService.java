package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(final String userId, Comparator<E> comparator);

    E findOneById(String userId, String id);

    E findOneByIndex(final String userId, final Integer index);

    void remove(String userId, E entity);

    E removeOneById(String userId, String id);

    E removeOneByIndex(final String userId, final Integer index);

}
