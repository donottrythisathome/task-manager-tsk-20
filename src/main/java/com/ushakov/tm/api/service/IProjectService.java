package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IOwnerBusinessService<Project> {

    Project add(String userId, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

    Project completeProjectById(String userId, String id);

    Project completeProjectByIndex(String userId, Integer index);

    Project completeProjectByName(String userId, String name);

    Project startProjectById(String userId, String id);

    Project startProjectByIndex(String userId, Integer index);

    Project startProjectByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

}
