package com.ushakov.tm.api;

import com.ushakov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
