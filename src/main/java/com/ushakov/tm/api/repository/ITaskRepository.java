package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractEntity;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}
