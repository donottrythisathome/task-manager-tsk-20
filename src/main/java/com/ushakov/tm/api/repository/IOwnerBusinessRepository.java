package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.AbstractOwnerBusinessEntity;

public interface IOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity> extends IOwnerRepository<E> {

    E findOneByName(final String userId, final String name);

    E removeOneByName(final String userId, final String name);

}
