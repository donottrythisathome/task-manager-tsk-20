package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, E entity);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(final String userId, Comparator<E> comparator);

    E findOneById(String userId, String id);

    E findOneByIndex(final String userId, final Integer index);

    E removeOneById(String userId, String id);

    E removeOneByIndex(final String userId, final Integer index);

}
