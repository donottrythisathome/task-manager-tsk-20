package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractEntity;
import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerBusinessRepository<Project> {
}
