package com.ushakov.tm.api;

import com.ushakov.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void clear();

    List<E> findAll();

    E findOneById(String id);

    void remove(E entity);

    E removeOneById(String id);

}
