package com.ushakov.tm.service;

import com.ushakov.tm.api.service.IAuthService;
import com.ushakov.tm.api.service.IUserService;
import com.ushakov.tm.exception.empty.EmptyLoginException;
import com.ushakov.tm.exception.empty.EmptyPasswordException;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.exception.auth.AccessDeniedException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;


public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.add(login, password, email);
    }

}
