package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task bindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Project deleteProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : taskList) {
            taskRepository.removeOneById(userId, task.getId());
        }
        return projectRepository.removeOneById(userId, projectId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
