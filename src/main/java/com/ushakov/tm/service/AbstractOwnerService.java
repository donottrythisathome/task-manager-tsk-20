package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.api.service.IOwnerService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerEntity;
import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E>
        implements IOwnerService<E> {

    private IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return null;
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findOneById(userId, id);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeOneByIndex(userId, index);
    }

}
