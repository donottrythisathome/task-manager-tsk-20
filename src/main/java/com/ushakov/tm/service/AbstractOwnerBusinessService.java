package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IOwnerBusinessRepository;
import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.api.service.IOwnerBusinessService;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;

public abstract class AbstractOwnerBusinessService<E extends AbstractOwnerBusinessEntity>
        extends AbstractOwnerService<E>
        implements IOwnerBusinessService<E> {

    private IOwnerBusinessRepository<E> repository;

    public AbstractOwnerBusinessService(IOwnerBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findOneByName(userId, name);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeOneByName(userId, name);
    }

}
